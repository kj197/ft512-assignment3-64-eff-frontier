CFLAGS= -Wall -Werror -std=gnu++98 -pedantic -ggdb3 -std=c++11

efficient_frontier: main.cpp
	g++ $(CFLAGS) -o efficient_frontier main.cpp

clean:
	rm -f test *~
