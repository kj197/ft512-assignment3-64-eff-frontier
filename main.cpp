#include <Eigen/Dense>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>  
#include <cstdlib>        
#include <string>
#include <stdlib.h>
#include <numeric>
#include<cmath>
#include <iomanip>
#include <math.h>
#include <getopt.h>
using namespace std;
using namespace Eigen;

MatrixXd openData(char* fileToOpen){

  std::ifstream f1(fileToOpen);

  if(!f1){
    fprintf(stderr,"file1 is null.");
    exit(EXIT_FAILURE);
  }

  vector<double> matrixEntries;
  string matrixRowString;
  string matrixEntry;
  int matrixRowNumber = 0;
  
  while (getline(f1, matrixRowString))
    {
      stringstream matrixRowStringStream(matrixRowString);
      
      while (getline(matrixRowStringStream, matrixEntry, ','))
        {
	  matrixEntries.push_back(atof(matrixEntry.c_str()));
	  //matrixEntries.push_back(stod(matrixEntry));
        }
      matrixRowNumber++;
    }
  f1.close();
  return Map<Matrix<double, Dynamic, Dynamic, RowMajor>>(matrixEntries.data(), matrixRowNumber, matrixEntries.size() / matrixRowNumber);
}




int main(int argc, char *argv[]){
  
  if(argc!=3 && argc!=4){
    fprintf(stderr,"input number is incorrect.");
    exit(EXIT_FAILURE);
  }

  MatrixXd corr;
  MatrixXd m2;
  if(argc==3){
    //read corr                                                                                                                                                                                                      
    corr=openData(argv[2]);
        
    //read r
    m2=openData(argv[1]);
 }
  
  if(argc==4){
    //read corr
    corr=openData(argv[3]);

    //read r
    m2=openData(argv[2]);
  }

  /************test cases**********/
  if(corr.rows()!=corr.cols()){
    fprintf(stderr,"corr is not a square.\n");
    exit(EXIT_FAILURE);
  }
  
  if(corr.rows()!=m2.rows()){
    fprintf(stderr,"corr size and asset numer don't match.\n");
    exit(EXIT_FAILURE);
  }

  for(int i =0;i<corr.rows();i++){
    for(int j=0; j<corr.cols();j++){
      if(isalpha(corr(i,j))){
	fprintf(stderr,"corr has non numeric.\n");
	exit(EXIT_FAILURE);
      }
    }
  }

  if(m2.cols()!=3){
    fprintf(stderr,"universe doesn't have 3 cols.\n");
    exit(EXIT_FAILURE);
  }
  
  /*****************/

  size_t size=m2.col(1).size();
  VectorXd r(size);
  r<<m2.col(1);
  
  //sigma
  MatrixXd sigma=corr;
  for(int i=0; i<corr.rows(); i++){
    for(int j=0;j<corr.cols();j++){
      sigma(i,j)=corr(i,j)*m2(i,2)*m2(j,2);
    }
  }
  // cout<<sigma<<endl;

  //1
  VectorXd ones = VectorXd :: Ones (size) ;
  //cout<<ones<<endl;

  //A
  MatrixXd A(2,size);
  A.row(0)=ones.transpose ();
  A.row(1)=r.transpose ();
  // cout<<A<<endl;

  //left                                                                                                                                                                                                             
   MatrixXd temp(sigma.rows(), sigma.cols()+A.transpose().cols());
   temp << sigma,A.transpose(); //[sigma, AT]                                                                                                                                                                         
   MatrixXd doubleZero = MatrixXd :: Zero (2,2) ;//0                                                                                                                                                                  
   MatrixXd A_temp(A.rows(),A.cols()+doubleZero.cols());
   A_temp<<A,doubleZero;//[A,0]                                                                                                                                                                                       
   MatrixXd left(temp.rows()+A.rows(),temp.cols());
   left<<temp,A_temp;
   //cout<<left<<endl;

   cout<<"ROR,volatility"<<endl;
   
   for(double rp=0.01;rp<=0.27;rp+=0.01){

     //b
     MatrixXd b(2,1);
     b<<1,
       rp;
     // cout<<b<<endl;
     
     //right
     MatrixXd zeros = MatrixXd :: Zero (size,1);//7*1 0s
     MatrixXd right(zeros.rows()+b.rows(), zeros.cols());
     right<<zeros,b;
     // cout<<right<<endl;
     
     //calculation
     MatrixXd res(size+2,1);
     res = left.fullPivHouseholderQr().solve(right);
     //cout<<res<<endl;
     
     /***********************step1******************/
     
     //get volatility                                                                                                                                                                                                
     double sum=0.0;
     for(size_t i=0; i<size; i++){
       for(size_t j=0;j<size;j++){
         sum+=res(i,0)*res(j,0)*sigma(i,j);
       }
     }

     if(argc==3){
       cout<<setprecision (1)<<fixed<<rp*100<<"%"<<",";
       cout<<setprecision (2)<<fixed <<sqrt(sum)*100<<"%"<<endl;
     }


     /************************step2******************/
     
     int neg_count=1;
     MatrixXd res2=res;
     MatrixXd left2=left;
     MatrixXd right2=right;
     
     for (int x=0;;x++){  
       neg_count=0;
       for(size_t i=0; i<size;i++){
         if(res2(i,0)<0){neg_count++;}
       }
       
       if (neg_count == 0) {
	 break;
       }
       
       //add new rows
       MatrixXd left2_down= MatrixXd :: Zero (neg_count,left2.cols());
       MatrixXd right2_down= MatrixXd :: Zero (neg_count,right2.cols());
       int i=0;
       for(size_t j=0;j<size;j++){
	 if(res2(j,0)<0) {
	   left2_down(i,j)=1;
	   i++;
	 }
       }
       // cout<<"left2_down is:"<<left2_down<<endl;
       
       MatrixXd O = MatrixXd :: Zero (neg_count,neg_count);
       
       MatrixXd left_new(left2.rows()+left2_down.rows(),left2.rows()+left2_down.rows());
       left_new<<left2,left2_down.transpose(),left2_down,O;
       //cout<<"left_new is:"<<left_new<<endl;
       
       MatrixXd right_new(right2.rows()+right2_down.rows(),right2.cols());
       right_new<<right2,right2_down;
       // cout<<"right_new is:"<<right_new<<endl<<endl;
       
       //calculate
       res2 = left_new.fullPivHouseholderQr().solve(right_new);
       // cout<<"now the res2 is: "<<res2<<endl<<endl;
       
       neg_count=0;
       for(size_t i=0; i<size;i++){
         if(res2(i,0)<0){neg_count++;}
       }
       //   cout<<"now neg_count is:"<<neg_count<<endl;
       
       left2.resize(left_new.rows(),left_new.cols());
       left2=left_new;
       right2.resize(right_new.rows(),right_new.cols());
       right2=right_new;
       
     }
     //variance
     double sum2=0.0;
     for(size_t i=0; i<size; i++){
       for(size_t j=0;j<size;j++){
	 sum2+=res2(i,0)*res2(j,0)*sigma(i,j);
       }
     }
     
     if(argc==4 && strcmp(argv[1], "-r") == 0){
       cout<<setprecision (1)<<fixed<<rp*100<<"%"<<",";
       cout<<setprecision (2)<<fixed <<sqrt(sum2)*100<<"%"<<endl;     
     }
   }
   
}


